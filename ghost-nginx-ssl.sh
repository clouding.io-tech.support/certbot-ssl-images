#!/bin/bash
# Clouding.io

# Move to install path
cd /var/www/ghost/

# Configure Domain in Ghost
su ghostinst -c "ghost config url https://$1"

# Restart Ghost
su ghostinst -c 'ghost restart'

# Update repos
apt update

# Install packages
apt install certbot python3-certbot-nginx -y

# Add domain
sed -i "s/"Ghost"/"$1"/g" /etc/nginx/sites-enabled/default

# Restart Nginx
systemctl restart nginx

# Register e-mail
yes | certbot --non-interactive register --agree-tos -m $2

# Create certificate
certbot --authenticator webroot -w /var/www/html/ --redirect --installer nginx -d $1
