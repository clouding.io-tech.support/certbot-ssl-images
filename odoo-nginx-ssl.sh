#!/bin/bash
# Clouding.io

UBUNTU_VER=`cat /etc/os-release | grep VERSION_ID | cut -d"=" -f2 | tr "\"" " " | tr -d ' '`

if [ $UBUNTU_VER == "20.04" ] || [ $UBUNTU_VER == "18.04" ] || [ $UBUNTU_VER == "22.04" ] || [ $UBUNTU_VER == "24.04" ]
then
	# Install packages for Ubuntu 18.04, 20.04, 22.04 and 24.04
	apt install -y certbot python3-certbot-nginx

elif [ $UBUNTU_VER == "16.04" ]
then
	# Add Repo
	add-apt-repository ppa:certbot/certbot
	# Update Repos
	apt update
	# Install Packages
	apt install -y python-certbot-nginx
else
	echo "Sorry! Your version is not supported"
	echo "Try installing SSL manually."
	exit 0
fi

# Add domain
sed -i "s/"odoo.mycompany.com"/"$1"/g" /etc/nginx/sites-enabled/default

# Restart Nginx
systemctl restart nginx

# Register e-mail
yes | certbot --non-interactive register --agree-tos -m $2

# Create certificate
certbot --authenticator webroot -w /var/www/html/ --redirect --installer nginx -d $1
