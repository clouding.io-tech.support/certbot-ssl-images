#!/bin/bash
# Clouding.io

UBUNTU_VER=`cat /etc/os-release | grep VERSION_ID | cut -d"=" -f2 | tr "\"" " " | tr -d ' '`

if [ -z "$1" ] || [ -z "$2" ]
then
        echo "Sorry, there seems to be some value missing in the command."
        echo "Usage: ./wp-nginx-ssl.sh [domain] [email] (www)"
        exit 0
fi

if [ $UBUNTU_VER == "20.04" ] || [ $UBUNTU_VER == "22.04" ]
then
        # Update and install packages for Ubuntu 20.04 and 22.04
        apt update
        apt install certbot python3-certbot-nginx -y
else
        echo "Sorry! Your version is not supported"
        echo "Try installing SSL manually or contact us at soporte@clouding.io."
        exit 0
fi

# Add domain in Nginx config
if [ -d $3 ]
then
        sed -i "s/^[[:space:]]*server_name[^;]*/server_name $1/" /etc/nginx/sites-enabled/default
else
        if [ $3 == "www" ]
        then
                sed -i "s/^[[:space:]]*server_name[^;]*/server_name $1 www.$1/" /etc/nginx/sites-enabled/default
        else
                sed -i "s/^[[:space:]]*server_name[^;]*/server_name $1/" /etc/nginx/sites-enabled/default
        fi
fi

# Restart Nginx
systemctl restart nginx

# Register e-mail
yes | certbot --non-interactive register --agree-tos -m $2

# Create certificate
if [ -d $3 ]
then
        certbot --authenticator webroot -w /var/www/html/prestashop/ --redirect --installer nginx -d $1
else
        certbot --authenticator webroot -w /var/www/html/prestashop/ --redirect --installer nginx -d $1 -d $3.$1
fi
